﻿namespace Generator_s_Names_Helper
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.whatisit_combobox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.WhatIHaveAlready_groupbox = new System.Windows.Forms.GroupBox();
            this.beastN = new System.Windows.Forms.TextBox();
            this.beardN = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.wingN = new System.Windows.Forms.TextBox();
            this.tailN = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.rearhairN = new System.Windows.Forms.TextBox();
            this.noseN = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.mouthN = new System.Windows.Forms.TextBox();
            this.glassesN = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.fronthairN = new System.Windows.Forms.TextBox();
            this.facialN = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.faceN = new System.Windows.Forms.TextBox();
            this.eyesN = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.eyebrowN = new System.Windows.Forms.TextBox();
            this.earsN = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.clothN = new System.Windows.Forms.TextBox();
            this.cloakN = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.accbN = new System.Windows.Forms.TextBox();
            this.accaN = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.fullPathTextBox = new System.Windows.Forms.TextBox();
            this.fg_groupbox = new System.Windows.Forms.GroupBox();
            this.fg_layer2_mask4_combobox = new System.Windows.Forms.ComboBox();
            this.fg_layer2_mask4_textbox = new System.Windows.Forms.TextBox();
            this.fg_layer2_mask3_combobox = new System.Windows.Forms.ComboBox();
            this.fg_layer2_mask3_textbox = new System.Windows.Forms.TextBox();
            this.fg_layer2_mask2_combobox = new System.Windows.Forms.ComboBox();
            this.fg_layer2_mask2_textbox = new System.Windows.Forms.TextBox();
            this.fg_layer2_mask1_combobox = new System.Windows.Forms.ComboBox();
            this.fg_layer2_mask1_textbox = new System.Windows.Forms.TextBox();
            this.fg_layer1_mask4_combobox = new System.Windows.Forms.ComboBox();
            this.fg_layer1_mask4_textbox = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.fg_layer1_mask3_combobox = new System.Windows.Forms.ComboBox();
            this.fg_layer1_mask3_textbox = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.fg_layer1_mask2_combobox = new System.Windows.Forms.ComboBox();
            this.fg_layer1_mask2_textbox = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.fg_layer1_mask1_combobox = new System.Windows.Forms.ComboBox();
            this.fg_layer1_mask1_textbox = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.fg_DefaultLayer_Label = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.bodysfolders_combobox = new System.Windows.Forms.ComboBox();
            this.sv_groupbox = new System.Windows.Forms.GroupBox();
            this.sv_layer2_mask_textbox = new System.Windows.Forms.TextBox();
            this.sv_layer1_mask_textbox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.sv_layer2_sprite_textbox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.sv_layer1_sprite_textbox = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.tv_groupbox = new System.Windows.Forms.GroupBox();
            this.tvd_damagedlayer_mask_textbox = new System.Windows.Forms.TextBox();
            this.tvd_damagedlayer_sprite_textbox = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.tv_layer2_mask_textbox = new System.Windows.Forms.TextBox();
            this.tv_layer1_mask_textbox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.tv_layer2_sprite_textbox = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.tv_layer1_sprite_textbox = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.icon_groupbox = new System.Windows.Forms.GroupBox();
            this.icon_sprite_textbox = new System.Windows.Forms.TextBox();
            this.generatorfolder_textbox = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.search_btn = new System.Windows.Forms.Button();
            this.AddNewIconBTN = new System.Windows.Forms.Button();
            this.StartCopy_Btn = new System.Windows.Forms.Button();
            this.label31 = new System.Windows.Forms.Label();
            this.CleanAll_Btn = new System.Windows.Forms.Button();
            this.WhatIHaveAlready_groupbox.SuspendLayout();
            this.fg_groupbox.SuspendLayout();
            this.sv_groupbox.SuspendLayout();
            this.tv_groupbox.SuspendLayout();
            this.icon_groupbox.SuspendLayout();
            this.SuspendLayout();
            // 
            // whatisit_combobox
            // 
            this.whatisit_combobox.Enabled = false;
            this.whatisit_combobox.FormattingEnabled = true;
            this.whatisit_combobox.Items.AddRange(new object[] {
            "AccA",
            "AccB",
            "Beard",
            "BeastEars",
            "Cloak",
            "Clothing",
            "Ears",
            "Eyebrows",
            "Eyes",
            "Face",
            "FacialMark",
            "FrontHair",
            "Glasses",
            "Mouth",
            "Nose",
            "RearHair",
            "Tail",
            "Wing"});
            this.whatisit_combobox.Location = new System.Drawing.Point(192, 168);
            this.whatisit_combobox.Name = "whatisit_combobox";
            this.whatisit_combobox.Size = new System.Drawing.Size(121, 21);
            this.whatisit_combobox.TabIndex = 0;
            this.whatisit_combobox.SelectedIndexChanged += new System.EventHandler(this.whatisit_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Ravie", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(24, 168);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(168, 24);
            this.label1.TabIndex = 7;
            this.label1.Text = "What is it?";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // WhatIHaveAlready_groupbox
            // 
            this.WhatIHaveAlready_groupbox.Controls.Add(this.beastN);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.beardN);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.label7);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.label8);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.wingN);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.tailN);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.label17);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.label18);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.rearhairN);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.noseN);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.label19);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.label20);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.mouthN);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.glassesN);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.label21);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.label22);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.fronthairN);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.facialN);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.label23);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.label24);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.faceN);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.eyesN);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.label15);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.label16);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.eyebrowN);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.earsN);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.label13);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.label14);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.clothN);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.cloakN);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.label11);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.label12);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.accbN);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.accaN);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.label10);
            this.WhatIHaveAlready_groupbox.Controls.Add(this.label9);
            this.WhatIHaveAlready_groupbox.Font = new System.Drawing.Font("Ravie", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WhatIHaveAlready_groupbox.Location = new System.Drawing.Point(824, 80);
            this.WhatIHaveAlready_groupbox.Name = "WhatIHaveAlready_groupbox";
            this.WhatIHaveAlready_groupbox.Size = new System.Drawing.Size(280, 616);
            this.WhatIHaveAlready_groupbox.TabIndex = 17;
            this.WhatIHaveAlready_groupbox.TabStop = false;
            this.WhatIHaveAlready_groupbox.Text = "What I have Already?";
            // 
            // beastN
            // 
            this.beastN.Enabled = false;
            this.beastN.Location = new System.Drawing.Point(160, 120);
            this.beastN.MaxLength = 3;
            this.beastN.Name = "beastN";
            this.beastN.Size = new System.Drawing.Size(96, 29);
            this.beastN.TabIndex = 44;
            this.beastN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // beardN
            // 
            this.beardN.Enabled = false;
            this.beardN.Location = new System.Drawing.Point(24, 120);
            this.beardN.MaxLength = 3;
            this.beardN.Name = "beardN";
            this.beardN.Size = new System.Drawing.Size(96, 29);
            this.beardN.TabIndex = 43;
            this.beardN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(144, 96);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(128, 24);
            this.label7.TabIndex = 42;
            this.label7.Text = "Beast Ears";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(8, 96);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(128, 24);
            this.label8.TabIndex = 41;
            this.label8.Text = "Beard";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // wingN
            // 
            this.wingN.Enabled = false;
            this.wingN.Location = new System.Drawing.Point(160, 568);
            this.wingN.MaxLength = 3;
            this.wingN.Name = "wingN";
            this.wingN.Size = new System.Drawing.Size(96, 29);
            this.wingN.TabIndex = 40;
            this.wingN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tailN
            // 
            this.tailN.Enabled = false;
            this.tailN.Location = new System.Drawing.Point(24, 568);
            this.tailN.MaxLength = 3;
            this.tailN.Name = "tailN";
            this.tailN.Size = new System.Drawing.Size(96, 29);
            this.tailN.TabIndex = 39;
            this.tailN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(144, 544);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(128, 24);
            this.label17.TabIndex = 38;
            this.label17.Text = "Wing";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(8, 544);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(128, 24);
            this.label18.TabIndex = 37;
            this.label18.Text = "Tail";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // rearhairN
            // 
            this.rearhairN.Enabled = false;
            this.rearhairN.Location = new System.Drawing.Point(160, 504);
            this.rearhairN.MaxLength = 3;
            this.rearhairN.Name = "rearhairN";
            this.rearhairN.Size = new System.Drawing.Size(96, 29);
            this.rearhairN.TabIndex = 36;
            this.rearhairN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // noseN
            // 
            this.noseN.Enabled = false;
            this.noseN.Location = new System.Drawing.Point(24, 504);
            this.noseN.MaxLength = 3;
            this.noseN.Name = "noseN";
            this.noseN.Size = new System.Drawing.Size(96, 29);
            this.noseN.TabIndex = 35;
            this.noseN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(144, 480);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(128, 24);
            this.label19.TabIndex = 34;
            this.label19.Text = "Rear Hair";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(8, 480);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(128, 24);
            this.label20.TabIndex = 33;
            this.label20.Text = "Nose";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mouthN
            // 
            this.mouthN.Enabled = false;
            this.mouthN.Location = new System.Drawing.Point(160, 440);
            this.mouthN.MaxLength = 3;
            this.mouthN.Name = "mouthN";
            this.mouthN.Size = new System.Drawing.Size(96, 29);
            this.mouthN.TabIndex = 32;
            this.mouthN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // glassesN
            // 
            this.glassesN.Enabled = false;
            this.glassesN.Location = new System.Drawing.Point(24, 440);
            this.glassesN.MaxLength = 3;
            this.glassesN.Name = "glassesN";
            this.glassesN.Size = new System.Drawing.Size(96, 29);
            this.glassesN.TabIndex = 31;
            this.glassesN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(144, 416);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(128, 24);
            this.label21.TabIndex = 30;
            this.label21.Text = "Mouth";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(8, 416);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(128, 24);
            this.label22.TabIndex = 29;
            this.label22.Text = "Glasses";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fronthairN
            // 
            this.fronthairN.Enabled = false;
            this.fronthairN.Location = new System.Drawing.Point(160, 376);
            this.fronthairN.MaxLength = 3;
            this.fronthairN.Name = "fronthairN";
            this.fronthairN.Size = new System.Drawing.Size(96, 29);
            this.fronthairN.TabIndex = 28;
            this.fronthairN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // facialN
            // 
            this.facialN.Enabled = false;
            this.facialN.Location = new System.Drawing.Point(24, 376);
            this.facialN.MaxLength = 3;
            this.facialN.Name = "facialN";
            this.facialN.Size = new System.Drawing.Size(96, 29);
            this.facialN.TabIndex = 27;
            this.facialN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(144, 352);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(128, 24);
            this.label23.TabIndex = 26;
            this.label23.Text = "Front Hair";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(8, 352);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(128, 24);
            this.label24.TabIndex = 25;
            this.label24.Text = "Facial Mark";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // faceN
            // 
            this.faceN.Enabled = false;
            this.faceN.Location = new System.Drawing.Point(160, 312);
            this.faceN.MaxLength = 3;
            this.faceN.Name = "faceN";
            this.faceN.Size = new System.Drawing.Size(96, 29);
            this.faceN.TabIndex = 24;
            this.faceN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // eyesN
            // 
            this.eyesN.Enabled = false;
            this.eyesN.Location = new System.Drawing.Point(24, 312);
            this.eyesN.MaxLength = 3;
            this.eyesN.Name = "eyesN";
            this.eyesN.Size = new System.Drawing.Size(96, 29);
            this.eyesN.TabIndex = 23;
            this.eyesN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(144, 288);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(128, 24);
            this.label15.TabIndex = 22;
            this.label15.Text = "Face";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(8, 288);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(128, 24);
            this.label16.TabIndex = 21;
            this.label16.Text = "Eyes";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // eyebrowN
            // 
            this.eyebrowN.Enabled = false;
            this.eyebrowN.Location = new System.Drawing.Point(160, 248);
            this.eyebrowN.MaxLength = 3;
            this.eyebrowN.Name = "eyebrowN";
            this.eyebrowN.Size = new System.Drawing.Size(96, 29);
            this.eyebrowN.TabIndex = 20;
            this.eyebrowN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // earsN
            // 
            this.earsN.Enabled = false;
            this.earsN.Location = new System.Drawing.Point(24, 248);
            this.earsN.MaxLength = 3;
            this.earsN.Name = "earsN";
            this.earsN.Size = new System.Drawing.Size(96, 29);
            this.earsN.TabIndex = 19;
            this.earsN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(144, 224);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(128, 24);
            this.label13.TabIndex = 18;
            this.label13.Text = "Eye Brows";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(8, 224);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(128, 24);
            this.label14.TabIndex = 17;
            this.label14.Text = "Ears";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // clothN
            // 
            this.clothN.Enabled = false;
            this.clothN.Location = new System.Drawing.Point(160, 184);
            this.clothN.MaxLength = 3;
            this.clothN.Name = "clothN";
            this.clothN.Size = new System.Drawing.Size(96, 29);
            this.clothN.TabIndex = 16;
            this.clothN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cloakN
            // 
            this.cloakN.Enabled = false;
            this.cloakN.Location = new System.Drawing.Point(24, 184);
            this.cloakN.MaxLength = 3;
            this.cloakN.Name = "cloakN";
            this.cloakN.Size = new System.Drawing.Size(96, 29);
            this.cloakN.TabIndex = 15;
            this.cloakN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(144, 160);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(128, 24);
            this.label11.TabIndex = 14;
            this.label11.Text = "Clothing";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(8, 160);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(128, 24);
            this.label12.TabIndex = 13;
            this.label12.Text = "Cloak";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // accbN
            // 
            this.accbN.Enabled = false;
            this.accbN.Location = new System.Drawing.Point(160, 56);
            this.accbN.MaxLength = 3;
            this.accbN.Name = "accbN";
            this.accbN.Size = new System.Drawing.Size(96, 29);
            this.accbN.TabIndex = 12;
            this.accbN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // accaN
            // 
            this.accaN.Enabled = false;
            this.accaN.Location = new System.Drawing.Point(24, 56);
            this.accaN.MaxLength = 3;
            this.accaN.Name = "accaN";
            this.accaN.Size = new System.Drawing.Size(96, 29);
            this.accaN.TabIndex = 11;
            this.accaN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(144, 32);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(128, 24);
            this.label10.TabIndex = 10;
            this.label10.Text = "AccB";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(8, 32);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(128, 24);
            this.label9.TabIndex = 9;
            this.label9.Text = "AccA";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label32
            // 
            this.label32.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(24, 88);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(536, 24);
            this.label32.TabIndex = 35;
            this.label32.Text = "Folder of your new resource (your first drag will change this)";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fullPathTextBox
            // 
            this.fullPathTextBox.Enabled = false;
            this.fullPathTextBox.Location = new System.Drawing.Point(24, 120);
            this.fullPathTextBox.Name = "fullPathTextBox";
            this.fullPathTextBox.Size = new System.Drawing.Size(536, 20);
            this.fullPathTextBox.TabIndex = 36;
            // 
            // fg_groupbox
            // 
            this.fg_groupbox.Controls.Add(this.fg_layer2_mask4_combobox);
            this.fg_groupbox.Controls.Add(this.fg_layer2_mask4_textbox);
            this.fg_groupbox.Controls.Add(this.fg_layer2_mask3_combobox);
            this.fg_groupbox.Controls.Add(this.fg_layer2_mask3_textbox);
            this.fg_groupbox.Controls.Add(this.fg_layer2_mask2_combobox);
            this.fg_groupbox.Controls.Add(this.fg_layer2_mask2_textbox);
            this.fg_groupbox.Controls.Add(this.fg_layer2_mask1_combobox);
            this.fg_groupbox.Controls.Add(this.fg_layer2_mask1_textbox);
            this.fg_groupbox.Controls.Add(this.fg_layer1_mask4_combobox);
            this.fg_groupbox.Controls.Add(this.fg_layer1_mask4_textbox);
            this.fg_groupbox.Controls.Add(this.label36);
            this.fg_groupbox.Controls.Add(this.fg_layer1_mask3_combobox);
            this.fg_groupbox.Controls.Add(this.fg_layer1_mask3_textbox);
            this.fg_groupbox.Controls.Add(this.label35);
            this.fg_groupbox.Controls.Add(this.fg_layer1_mask2_combobox);
            this.fg_groupbox.Controls.Add(this.fg_layer1_mask2_textbox);
            this.fg_groupbox.Controls.Add(this.label34);
            this.fg_groupbox.Controls.Add(this.fg_layer1_mask1_combobox);
            this.fg_groupbox.Controls.Add(this.fg_layer1_mask1_textbox);
            this.fg_groupbox.Controls.Add(this.label37);
            this.fg_groupbox.Controls.Add(this.label33);
            this.fg_groupbox.Controls.Add(this.fg_DefaultLayer_Label);
            this.fg_groupbox.Font = new System.Drawing.Font("Ravie", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fg_groupbox.Location = new System.Drawing.Point(24, 216);
            this.fg_groupbox.Name = "fg_groupbox";
            this.fg_groupbox.Size = new System.Drawing.Size(576, 200);
            this.fg_groupbox.TabIndex = 37;
            this.fg_groupbox.TabStop = false;
            this.fg_groupbox.Text = "Face";
            // 
            // fg_layer2_mask4_combobox
            // 
            this.fg_layer2_mask4_combobox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.fg_layer2_mask4_combobox.FormattingEnabled = true;
            this.fg_layer2_mask4_combobox.Location = new System.Drawing.Point(472, 152);
            this.fg_layer2_mask4_combobox.Name = "fg_layer2_mask4_combobox";
            this.fg_layer2_mask4_combobox.Size = new System.Drawing.Size(96, 21);
            this.fg_layer2_mask4_combobox.TabIndex = 33;
            // 
            // fg_layer2_mask4_textbox
            // 
            this.fg_layer2_mask4_textbox.AllowDrop = true;
            this.fg_layer2_mask4_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.fg_layer2_mask4_textbox.Location = new System.Drawing.Point(472, 128);
            this.fg_layer2_mask4_textbox.Name = "fg_layer2_mask4_textbox";
            this.fg_layer2_mask4_textbox.Size = new System.Drawing.Size(96, 20);
            this.fg_layer2_mask4_textbox.TabIndex = 32;
            this.fg_layer2_mask4_textbox.DragDrop += new System.Windows.Forms.DragEventHandler(this.DragDropImage);
            this.fg_layer2_mask4_textbox.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterImage);
            // 
            // fg_layer2_mask3_combobox
            // 
            this.fg_layer2_mask3_combobox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.fg_layer2_mask3_combobox.FormattingEnabled = true;
            this.fg_layer2_mask3_combobox.Location = new System.Drawing.Point(360, 152);
            this.fg_layer2_mask3_combobox.Name = "fg_layer2_mask3_combobox";
            this.fg_layer2_mask3_combobox.Size = new System.Drawing.Size(96, 21);
            this.fg_layer2_mask3_combobox.TabIndex = 31;
            // 
            // fg_layer2_mask3_textbox
            // 
            this.fg_layer2_mask3_textbox.AllowDrop = true;
            this.fg_layer2_mask3_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.fg_layer2_mask3_textbox.Location = new System.Drawing.Point(360, 128);
            this.fg_layer2_mask3_textbox.Name = "fg_layer2_mask3_textbox";
            this.fg_layer2_mask3_textbox.Size = new System.Drawing.Size(96, 20);
            this.fg_layer2_mask3_textbox.TabIndex = 30;
            this.fg_layer2_mask3_textbox.DragDrop += new System.Windows.Forms.DragEventHandler(this.DragDropImage);
            this.fg_layer2_mask3_textbox.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterImage);
            // 
            // fg_layer2_mask2_combobox
            // 
            this.fg_layer2_mask2_combobox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.fg_layer2_mask2_combobox.FormattingEnabled = true;
            this.fg_layer2_mask2_combobox.Location = new System.Drawing.Point(248, 152);
            this.fg_layer2_mask2_combobox.Name = "fg_layer2_mask2_combobox";
            this.fg_layer2_mask2_combobox.Size = new System.Drawing.Size(96, 21);
            this.fg_layer2_mask2_combobox.TabIndex = 29;
            // 
            // fg_layer2_mask2_textbox
            // 
            this.fg_layer2_mask2_textbox.AllowDrop = true;
            this.fg_layer2_mask2_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.fg_layer2_mask2_textbox.Location = new System.Drawing.Point(248, 128);
            this.fg_layer2_mask2_textbox.Name = "fg_layer2_mask2_textbox";
            this.fg_layer2_mask2_textbox.Size = new System.Drawing.Size(96, 20);
            this.fg_layer2_mask2_textbox.TabIndex = 28;
            this.fg_layer2_mask2_textbox.DragDrop += new System.Windows.Forms.DragEventHandler(this.DragDropImage);
            this.fg_layer2_mask2_textbox.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterImage);
            // 
            // fg_layer2_mask1_combobox
            // 
            this.fg_layer2_mask1_combobox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.fg_layer2_mask1_combobox.FormattingEnabled = true;
            this.fg_layer2_mask1_combobox.Location = new System.Drawing.Point(136, 152);
            this.fg_layer2_mask1_combobox.Name = "fg_layer2_mask1_combobox";
            this.fg_layer2_mask1_combobox.Size = new System.Drawing.Size(96, 21);
            this.fg_layer2_mask1_combobox.TabIndex = 27;
            // 
            // fg_layer2_mask1_textbox
            // 
            this.fg_layer2_mask1_textbox.AllowDrop = true;
            this.fg_layer2_mask1_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.fg_layer2_mask1_textbox.Location = new System.Drawing.Point(136, 128);
            this.fg_layer2_mask1_textbox.Name = "fg_layer2_mask1_textbox";
            this.fg_layer2_mask1_textbox.Size = new System.Drawing.Size(96, 20);
            this.fg_layer2_mask1_textbox.TabIndex = 26;
            this.fg_layer2_mask1_textbox.DragDrop += new System.Windows.Forms.DragEventHandler(this.DragDropImage);
            this.fg_layer2_mask1_textbox.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterImage);
            // 
            // fg_layer1_mask4_combobox
            // 
            this.fg_layer1_mask4_combobox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.fg_layer1_mask4_combobox.FormattingEnabled = true;
            this.fg_layer1_mask4_combobox.Location = new System.Drawing.Point(472, 88);
            this.fg_layer1_mask4_combobox.Name = "fg_layer1_mask4_combobox";
            this.fg_layer1_mask4_combobox.Size = new System.Drawing.Size(96, 21);
            this.fg_layer1_mask4_combobox.TabIndex = 25;
            // 
            // fg_layer1_mask4_textbox
            // 
            this.fg_layer1_mask4_textbox.AllowDrop = true;
            this.fg_layer1_mask4_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.fg_layer1_mask4_textbox.Location = new System.Drawing.Point(472, 64);
            this.fg_layer1_mask4_textbox.Name = "fg_layer1_mask4_textbox";
            this.fg_layer1_mask4_textbox.Size = new System.Drawing.Size(96, 20);
            this.fg_layer1_mask4_textbox.TabIndex = 24;
            this.fg_layer1_mask4_textbox.DragDrop += new System.Windows.Forms.DragEventHandler(this.DragDropImage);
            this.fg_layer1_mask4_textbox.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterImage);
            // 
            // label36
            // 
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(464, 24);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(104, 20);
            this.label36.TabIndex = 23;
            this.label36.Text = "Mask 4";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fg_layer1_mask3_combobox
            // 
            this.fg_layer1_mask3_combobox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.fg_layer1_mask3_combobox.FormattingEnabled = true;
            this.fg_layer1_mask3_combobox.Location = new System.Drawing.Point(360, 88);
            this.fg_layer1_mask3_combobox.Name = "fg_layer1_mask3_combobox";
            this.fg_layer1_mask3_combobox.Size = new System.Drawing.Size(96, 21);
            this.fg_layer1_mask3_combobox.TabIndex = 22;
            // 
            // fg_layer1_mask3_textbox
            // 
            this.fg_layer1_mask3_textbox.AllowDrop = true;
            this.fg_layer1_mask3_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.fg_layer1_mask3_textbox.Location = new System.Drawing.Point(360, 64);
            this.fg_layer1_mask3_textbox.Name = "fg_layer1_mask3_textbox";
            this.fg_layer1_mask3_textbox.Size = new System.Drawing.Size(96, 20);
            this.fg_layer1_mask3_textbox.TabIndex = 21;
            this.fg_layer1_mask3_textbox.DragDrop += new System.Windows.Forms.DragEventHandler(this.DragDropImage);
            this.fg_layer1_mask3_textbox.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterImage);
            // 
            // label35
            // 
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(352, 24);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(120, 20);
            this.label35.TabIndex = 20;
            this.label35.Text = "Mask 3";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fg_layer1_mask2_combobox
            // 
            this.fg_layer1_mask2_combobox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.fg_layer1_mask2_combobox.FormattingEnabled = true;
            this.fg_layer1_mask2_combobox.Location = new System.Drawing.Point(248, 88);
            this.fg_layer1_mask2_combobox.Name = "fg_layer1_mask2_combobox";
            this.fg_layer1_mask2_combobox.Size = new System.Drawing.Size(96, 21);
            this.fg_layer1_mask2_combobox.TabIndex = 19;
            // 
            // fg_layer1_mask2_textbox
            // 
            this.fg_layer1_mask2_textbox.AllowDrop = true;
            this.fg_layer1_mask2_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.fg_layer1_mask2_textbox.Location = new System.Drawing.Point(248, 64);
            this.fg_layer1_mask2_textbox.Name = "fg_layer1_mask2_textbox";
            this.fg_layer1_mask2_textbox.Size = new System.Drawing.Size(96, 20);
            this.fg_layer1_mask2_textbox.TabIndex = 18;
            this.fg_layer1_mask2_textbox.DragDrop += new System.Windows.Forms.DragEventHandler(this.DragDropImage);
            this.fg_layer1_mask2_textbox.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterImage);
            // 
            // label34
            // 
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(240, 24);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(120, 20);
            this.label34.TabIndex = 17;
            this.label34.Text = "Mask 2";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fg_layer1_mask1_combobox
            // 
            this.fg_layer1_mask1_combobox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.fg_layer1_mask1_combobox.FormattingEnabled = true;
            this.fg_layer1_mask1_combobox.Location = new System.Drawing.Point(136, 88);
            this.fg_layer1_mask1_combobox.Name = "fg_layer1_mask1_combobox";
            this.fg_layer1_mask1_combobox.Size = new System.Drawing.Size(96, 21);
            this.fg_layer1_mask1_combobox.TabIndex = 16;
            // 
            // fg_layer1_mask1_textbox
            // 
            this.fg_layer1_mask1_textbox.AllowDrop = true;
            this.fg_layer1_mask1_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.fg_layer1_mask1_textbox.Location = new System.Drawing.Point(136, 64);
            this.fg_layer1_mask1_textbox.Name = "fg_layer1_mask1_textbox";
            this.fg_layer1_mask1_textbox.Size = new System.Drawing.Size(96, 20);
            this.fg_layer1_mask1_textbox.TabIndex = 15;
            this.fg_layer1_mask1_textbox.DragDrop += new System.Windows.Forms.DragEventHandler(this.DragDropImage);
            this.fg_layer1_mask1_textbox.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterImage);
            // 
            // label37
            // 
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(8, 136);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(120, 20);
            this.label37.TabIndex = 14;
            this.label37.Text = "Extra Layer";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label33
            // 
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(128, 24);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(120, 20);
            this.label33.TabIndex = 2;
            this.label33.Text = "Mask 1";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fg_DefaultLayer_Label
            // 
            this.fg_DefaultLayer_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fg_DefaultLayer_Label.Location = new System.Drawing.Point(8, 72);
            this.fg_DefaultLayer_Label.Name = "fg_DefaultLayer_Label";
            this.fg_DefaultLayer_Label.Size = new System.Drawing.Size(120, 20);
            this.fg_DefaultLayer_Label.TabIndex = 1;
            this.fg_DefaultLayer_Label.Text = "Default Layer";
            this.fg_DefaultLayer_Label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Ravie", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(888, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(168, 24);
            this.label2.TabIndex = 39;
            this.label2.Text = "Body";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bodysfolders_combobox
            // 
            this.bodysfolders_combobox.Enabled = false;
            this.bodysfolders_combobox.FormattingEnabled = true;
            this.bodysfolders_combobox.Items.AddRange(new object[] {
            "Still",
            "Not",
            "Implemented",
            "Be",
            "Patient"});
            this.bodysfolders_combobox.Location = new System.Drawing.Point(912, 40);
            this.bodysfolders_combobox.Name = "bodysfolders_combobox";
            this.bodysfolders_combobox.Size = new System.Drawing.Size(121, 21);
            this.bodysfolders_combobox.TabIndex = 38;
            this.bodysfolders_combobox.SelectedIndexChanged += new System.EventHandler(this.resourcefolders_combobox_SelectedIndexChanged);
            // 
            // sv_groupbox
            // 
            this.sv_groupbox.Controls.Add(this.sv_layer2_mask_textbox);
            this.sv_groupbox.Controls.Add(this.sv_layer1_mask_textbox);
            this.sv_groupbox.Controls.Add(this.label3);
            this.sv_groupbox.Controls.Add(this.label4);
            this.sv_groupbox.Controls.Add(this.sv_layer2_sprite_textbox);
            this.sv_groupbox.Controls.Add(this.label5);
            this.sv_groupbox.Controls.Add(this.sv_layer1_sprite_textbox);
            this.sv_groupbox.Controls.Add(this.label25);
            this.sv_groupbox.Font = new System.Drawing.Font("Ravie", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sv_groupbox.Location = new System.Drawing.Point(24, 424);
            this.sv_groupbox.Name = "sv_groupbox";
            this.sv_groupbox.Size = new System.Drawing.Size(376, 176);
            this.sv_groupbox.TabIndex = 40;
            this.sv_groupbox.TabStop = false;
            this.sv_groupbox.Text = "Battle Sprites";
            // 
            // sv_layer2_mask_textbox
            // 
            this.sv_layer2_mask_textbox.AllowDrop = true;
            this.sv_layer2_mask_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.sv_layer2_mask_textbox.Location = new System.Drawing.Point(264, 120);
            this.sv_layer2_mask_textbox.Name = "sv_layer2_mask_textbox";
            this.sv_layer2_mask_textbox.Size = new System.Drawing.Size(96, 20);
            this.sv_layer2_mask_textbox.TabIndex = 22;
            this.sv_layer2_mask_textbox.DragDrop += new System.Windows.Forms.DragEventHandler(this.DragDropImage);
            this.sv_layer2_mask_textbox.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterImage);
            // 
            // sv_layer1_mask_textbox
            // 
            this.sv_layer1_mask_textbox.AllowDrop = true;
            this.sv_layer1_mask_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.sv_layer1_mask_textbox.Location = new System.Drawing.Point(264, 96);
            this.sv_layer1_mask_textbox.Name = "sv_layer1_mask_textbox";
            this.sv_layer1_mask_textbox.Size = new System.Drawing.Size(96, 20);
            this.sv_layer1_mask_textbox.TabIndex = 21;
            this.sv_layer1_mask_textbox.DragDrop += new System.Windows.Forms.DragEventHandler(this.DragDropImage);
            this.sv_layer1_mask_textbox.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterImage);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(248, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 20);
            this.label3.TabIndex = 20;
            this.label3.Text = "Mask";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(136, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 20);
            this.label4.TabIndex = 19;
            this.label4.Text = "Sprite";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // sv_layer2_sprite_textbox
            // 
            this.sv_layer2_sprite_textbox.AllowDrop = true;
            this.sv_layer2_sprite_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.sv_layer2_sprite_textbox.Location = new System.Drawing.Point(144, 120);
            this.sv_layer2_sprite_textbox.Name = "sv_layer2_sprite_textbox";
            this.sv_layer2_sprite_textbox.Size = new System.Drawing.Size(96, 20);
            this.sv_layer2_sprite_textbox.TabIndex = 18;
            this.sv_layer2_sprite_textbox.DragDrop += new System.Windows.Forms.DragEventHandler(this.DragDropImage);
            this.sv_layer2_sprite_textbox.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterImage);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(16, 120);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 20);
            this.label5.TabIndex = 17;
            this.label5.Text = "Extra Layer";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // sv_layer1_sprite_textbox
            // 
            this.sv_layer1_sprite_textbox.AllowDrop = true;
            this.sv_layer1_sprite_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.sv_layer1_sprite_textbox.Location = new System.Drawing.Point(144, 96);
            this.sv_layer1_sprite_textbox.Name = "sv_layer1_sprite_textbox";
            this.sv_layer1_sprite_textbox.Size = new System.Drawing.Size(96, 20);
            this.sv_layer1_sprite_textbox.TabIndex = 15;
            this.sv_layer1_sprite_textbox.DragDrop += new System.Windows.Forms.DragEventHandler(this.DragDropImage);
            this.sv_layer1_sprite_textbox.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterImage);
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(16, 96);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(120, 20);
            this.label25.TabIndex = 2;
            this.label25.Text = "Default Layer";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tv_groupbox
            // 
            this.tv_groupbox.Controls.Add(this.tvd_damagedlayer_mask_textbox);
            this.tv_groupbox.Controls.Add(this.tvd_damagedlayer_sprite_textbox);
            this.tv_groupbox.Controls.Add(this.label29);
            this.tv_groupbox.Controls.Add(this.tv_layer2_mask_textbox);
            this.tv_groupbox.Controls.Add(this.tv_layer1_mask_textbox);
            this.tv_groupbox.Controls.Add(this.label6);
            this.tv_groupbox.Controls.Add(this.label26);
            this.tv_groupbox.Controls.Add(this.tv_layer2_sprite_textbox);
            this.tv_groupbox.Controls.Add(this.label27);
            this.tv_groupbox.Controls.Add(this.tv_layer1_sprite_textbox);
            this.tv_groupbox.Controls.Add(this.label28);
            this.tv_groupbox.Font = new System.Drawing.Font("Ravie", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tv_groupbox.Location = new System.Drawing.Point(424, 424);
            this.tv_groupbox.Name = "tv_groupbox";
            this.tv_groupbox.Size = new System.Drawing.Size(376, 176);
            this.tv_groupbox.TabIndex = 41;
            this.tv_groupbox.TabStop = false;
            this.tv_groupbox.Text = "Character";
            // 
            // tvd_damagedlayer_mask_textbox
            // 
            this.tvd_damagedlayer_mask_textbox.AllowDrop = true;
            this.tvd_damagedlayer_mask_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.tvd_damagedlayer_mask_textbox.Location = new System.Drawing.Point(264, 144);
            this.tvd_damagedlayer_mask_textbox.Name = "tvd_damagedlayer_mask_textbox";
            this.tvd_damagedlayer_mask_textbox.Size = new System.Drawing.Size(96, 20);
            this.tvd_damagedlayer_mask_textbox.TabIndex = 25;
            this.tvd_damagedlayer_mask_textbox.DragDrop += new System.Windows.Forms.DragEventHandler(this.DragDropImage);
            this.tvd_damagedlayer_mask_textbox.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterImage);
            // 
            // tvd_damagedlayer_sprite_textbox
            // 
            this.tvd_damagedlayer_sprite_textbox.AllowDrop = true;
            this.tvd_damagedlayer_sprite_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.tvd_damagedlayer_sprite_textbox.Location = new System.Drawing.Point(144, 144);
            this.tvd_damagedlayer_sprite_textbox.Name = "tvd_damagedlayer_sprite_textbox";
            this.tvd_damagedlayer_sprite_textbox.Size = new System.Drawing.Size(96, 20);
            this.tvd_damagedlayer_sprite_textbox.TabIndex = 24;
            this.tvd_damagedlayer_sprite_textbox.DragDrop += new System.Windows.Forms.DragEventHandler(this.DragDropImage);
            this.tvd_damagedlayer_sprite_textbox.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterImage);
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(16, 144);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(120, 20);
            this.label29.TabIndex = 23;
            this.label29.Text = "Damaged Layer";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tv_layer2_mask_textbox
            // 
            this.tv_layer2_mask_textbox.AllowDrop = true;
            this.tv_layer2_mask_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.tv_layer2_mask_textbox.Location = new System.Drawing.Point(264, 104);
            this.tv_layer2_mask_textbox.Name = "tv_layer2_mask_textbox";
            this.tv_layer2_mask_textbox.Size = new System.Drawing.Size(96, 20);
            this.tv_layer2_mask_textbox.TabIndex = 22;
            this.tv_layer2_mask_textbox.DragDrop += new System.Windows.Forms.DragEventHandler(this.DragDropImage);
            this.tv_layer2_mask_textbox.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterImage);
            // 
            // tv_layer1_mask_textbox
            // 
            this.tv_layer1_mask_textbox.AllowDrop = true;
            this.tv_layer1_mask_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.tv_layer1_mask_textbox.Location = new System.Drawing.Point(264, 80);
            this.tv_layer1_mask_textbox.Name = "tv_layer1_mask_textbox";
            this.tv_layer1_mask_textbox.Size = new System.Drawing.Size(96, 20);
            this.tv_layer1_mask_textbox.TabIndex = 21;
            this.tv_layer1_mask_textbox.DragDrop += new System.Windows.Forms.DragEventHandler(this.DragDropImage);
            this.tv_layer1_mask_textbox.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterImage);
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(248, 40);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 20);
            this.label6.TabIndex = 20;
            this.label6.Text = "Mask";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(136, 40);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(120, 20);
            this.label26.TabIndex = 19;
            this.label26.Text = "Sprite";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tv_layer2_sprite_textbox
            // 
            this.tv_layer2_sprite_textbox.AllowDrop = true;
            this.tv_layer2_sprite_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.tv_layer2_sprite_textbox.Location = new System.Drawing.Point(144, 104);
            this.tv_layer2_sprite_textbox.Name = "tv_layer2_sprite_textbox";
            this.tv_layer2_sprite_textbox.Size = new System.Drawing.Size(96, 20);
            this.tv_layer2_sprite_textbox.TabIndex = 18;
            this.tv_layer2_sprite_textbox.DragDrop += new System.Windows.Forms.DragEventHandler(this.DragDropImage);
            this.tv_layer2_sprite_textbox.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterImage);
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(16, 104);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(120, 20);
            this.label27.TabIndex = 17;
            this.label27.Text = "Extra Layer";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tv_layer1_sprite_textbox
            // 
            this.tv_layer1_sprite_textbox.AllowDrop = true;
            this.tv_layer1_sprite_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.tv_layer1_sprite_textbox.Location = new System.Drawing.Point(144, 80);
            this.tv_layer1_sprite_textbox.Name = "tv_layer1_sprite_textbox";
            this.tv_layer1_sprite_textbox.Size = new System.Drawing.Size(96, 20);
            this.tv_layer1_sprite_textbox.TabIndex = 15;
            this.tv_layer1_sprite_textbox.DragDrop += new System.Windows.Forms.DragEventHandler(this.DragDropImage);
            this.tv_layer1_sprite_textbox.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterImage);
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(16, 80);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(120, 20);
            this.label28.TabIndex = 2;
            this.label28.Text = "Default Layer";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // icon_groupbox
            // 
            this.icon_groupbox.Controls.Add(this.icon_sprite_textbox);
            this.icon_groupbox.Font = new System.Drawing.Font("Ravie", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.icon_groupbox.Location = new System.Drawing.Point(624, 216);
            this.icon_groupbox.Name = "icon_groupbox";
            this.icon_groupbox.Size = new System.Drawing.Size(176, 184);
            this.icon_groupbox.TabIndex = 42;
            this.icon_groupbox.TabStop = false;
            this.icon_groupbox.Text = "Icon";
            // 
            // icon_sprite_textbox
            // 
            this.icon_sprite_textbox.AllowDrop = true;
            this.icon_sprite_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.icon_sprite_textbox.Location = new System.Drawing.Point(40, 96);
            this.icon_sprite_textbox.Name = "icon_sprite_textbox";
            this.icon_sprite_textbox.Size = new System.Drawing.Size(96, 20);
            this.icon_sprite_textbox.TabIndex = 15;
            this.icon_sprite_textbox.DragDrop += new System.Windows.Forms.DragEventHandler(this.DragDropImage);
            this.icon_sprite_textbox.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterImage);
            // 
            // generatorfolder_textbox
            // 
            this.generatorfolder_textbox.Enabled = false;
            this.generatorfolder_textbox.Location = new System.Drawing.Point(24, 48);
            this.generatorfolder_textbox.Name = "generatorfolder_textbox";
            this.generatorfolder_textbox.Size = new System.Drawing.Size(456, 20);
            this.generatorfolder_textbox.TabIndex = 43;
            // 
            // label30
            // 
            this.label30.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(24, 16);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(536, 24);
            this.label30.TabIndex = 44;
            this.label30.Text = "Generator Folder";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // search_btn
            // 
            this.search_btn.Location = new System.Drawing.Point(488, 48);
            this.search_btn.Name = "search_btn";
            this.search_btn.Size = new System.Drawing.Size(75, 23);
            this.search_btn.TabIndex = 45;
            this.search_btn.Text = "Search";
            this.search_btn.UseVisualStyleBackColor = true;
            this.search_btn.Click += new System.EventHandler(this.search_btn_Click);
            // 
            // AddNewIconBTN
            // 
            this.AddNewIconBTN.Enabled = false;
            this.AddNewIconBTN.Location = new System.Drawing.Point(640, 40);
            this.AddNewIconBTN.Name = "AddNewIconBTN";
            this.AddNewIconBTN.Size = new System.Drawing.Size(131, 104);
            this.AddNewIconBTN.TabIndex = 46;
            this.AddNewIconBTN.Text = "Add a NEW Icon for separate new items from old ones";
            this.AddNewIconBTN.UseVisualStyleBackColor = true;
            this.AddNewIconBTN.Click += new System.EventHandler(this.AddNewIconBTN_Click);
            // 
            // StartCopy_Btn
            // 
            this.StartCopy_Btn.Font = new System.Drawing.Font("Wide Latin", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StartCopy_Btn.Location = new System.Drawing.Point(24, 608);
            this.StartCopy_Btn.Name = "StartCopy_Btn";
            this.StartCopy_Btn.Size = new System.Drawing.Size(776, 80);
            this.StartCopy_Btn.TabIndex = 47;
            this.StartCopy_Btn.Text = "START COPYING!";
            this.StartCopy_Btn.UseVisualStyleBackColor = true;
            this.StartCopy_Btn.Click += new System.EventHandler(this.StartCopy_Btn_Click);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(904, 696);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(173, 13);
            this.label31.TabIndex = 48;
            this.label31.Text = "Made by Vegetamaker. Version 1.0";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // CleanAll_Btn
            // 
            this.CleanAll_Btn.Enabled = false;
            this.CleanAll_Btn.Location = new System.Drawing.Point(328, 160);
            this.CleanAll_Btn.Name = "CleanAll_Btn";
            this.CleanAll_Btn.Size = new System.Drawing.Size(200, 32);
            this.CleanAll_Btn.TabIndex = 49;
            this.CleanAll_Btn.Text = "Clean all fields";
            this.CleanAll_Btn.UseVisualStyleBackColor = true;
            this.CleanAll_Btn.Click += new System.EventHandler(this.CleanAll_Btn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1112, 721);
            this.Controls.Add(this.CleanAll_Btn);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.StartCopy_Btn);
            this.Controls.Add(this.AddNewIconBTN);
            this.Controls.Add(this.search_btn);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.generatorfolder_textbox);
            this.Controls.Add(this.icon_groupbox);
            this.Controls.Add(this.tv_groupbox);
            this.Controls.Add(this.sv_groupbox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.bodysfolders_combobox);
            this.Controls.Add(this.fg_groupbox);
            this.Controls.Add(this.fullPathTextBox);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.WhatIHaveAlready_groupbox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.whatisit_combobox);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Generator\'s Names Helper for RPG Maker MV";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.WhatIHaveAlready_groupbox.ResumeLayout(false);
            this.WhatIHaveAlready_groupbox.PerformLayout();
            this.fg_groupbox.ResumeLayout(false);
            this.fg_groupbox.PerformLayout();
            this.sv_groupbox.ResumeLayout(false);
            this.sv_groupbox.PerformLayout();
            this.tv_groupbox.ResumeLayout(false);
            this.tv_groupbox.PerformLayout();
            this.icon_groupbox.ResumeLayout(false);
            this.icon_groupbox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox whatisit_combobox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox WhatIHaveAlready_groupbox;
        private System.Windows.Forms.TextBox rearhairN;
        private System.Windows.Forms.TextBox noseN;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox mouthN;
        private System.Windows.Forms.TextBox glassesN;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox fronthairN;
        private System.Windows.Forms.TextBox facialN;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox faceN;
        private System.Windows.Forms.TextBox eyesN;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox eyebrowN;
        private System.Windows.Forms.TextBox earsN;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox clothN;
        private System.Windows.Forms.TextBox cloakN;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox accbN;
        private System.Windows.Forms.TextBox accaN;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox wingN;
        private System.Windows.Forms.TextBox tailN;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox beastN;
        private System.Windows.Forms.TextBox beardN;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox fullPathTextBox;
        private System.Windows.Forms.GroupBox fg_groupbox;
        private System.Windows.Forms.Label fg_DefaultLayer_Label;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox fg_layer1_mask1_textbox;
        private System.Windows.Forms.ComboBox fg_layer1_mask2_combobox;
        private System.Windows.Forms.TextBox fg_layer1_mask2_textbox;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.ComboBox fg_layer1_mask1_combobox;
        private System.Windows.Forms.ComboBox fg_layer2_mask4_combobox;
        private System.Windows.Forms.TextBox fg_layer2_mask4_textbox;
        private System.Windows.Forms.ComboBox fg_layer2_mask3_combobox;
        private System.Windows.Forms.TextBox fg_layer2_mask3_textbox;
        private System.Windows.Forms.ComboBox fg_layer2_mask2_combobox;
        private System.Windows.Forms.TextBox fg_layer2_mask2_textbox;
        private System.Windows.Forms.ComboBox fg_layer2_mask1_combobox;
        private System.Windows.Forms.TextBox fg_layer2_mask1_textbox;
        private System.Windows.Forms.ComboBox fg_layer1_mask4_combobox;
        private System.Windows.Forms.TextBox fg_layer1_mask4_textbox;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ComboBox fg_layer1_mask3_combobox;
        private System.Windows.Forms.TextBox fg_layer1_mask3_textbox;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox bodysfolders_combobox;
        private System.Windows.Forms.GroupBox sv_groupbox;
        private System.Windows.Forms.TextBox sv_layer2_sprite_textbox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox sv_layer1_sprite_textbox;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox sv_layer2_mask_textbox;
        private System.Windows.Forms.TextBox sv_layer1_mask_textbox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox tv_groupbox;
        private System.Windows.Forms.TextBox tv_layer2_mask_textbox;
        private System.Windows.Forms.TextBox tv_layer1_mask_textbox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox tv_layer2_sprite_textbox;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox tv_layer1_sprite_textbox;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox tvd_damagedlayer_mask_textbox;
        private System.Windows.Forms.TextBox tvd_damagedlayer_sprite_textbox;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.GroupBox icon_groupbox;
        private System.Windows.Forms.TextBox icon_sprite_textbox;
        private System.Windows.Forms.TextBox generatorfolder_textbox;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button search_btn;
        private System.Windows.Forms.Button AddNewIconBTN;
        private System.Windows.Forms.Button StartCopy_Btn;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button CleanAll_Btn;
    }
}

