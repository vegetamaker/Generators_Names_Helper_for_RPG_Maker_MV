﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Generator_s_Names_Helper
{
    public partial class Form1 : Form
    {
        bool debugMode = false;

        public Form1()
        {
            InitializeComponent();
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ResetAll(fg_groupbox);
            ResetAll(sv_groupbox);
            ResetAll(tv_groupbox);
            ResetAll(icon_groupbox);

            CheckingGeneratorFolderFile();
        }

        //When Combobox whatisit change. 
        private void whatisit_SelectedIndexChanged(object sender, EventArgs e)
        {
            fg_enabling(whatisit_combobox.Text);
            sv_enabling(whatisit_combobox.Text);
            tv_enabling(whatisit_combobox.Text);
            icon_enabling(whatisit_combobox.Text);
        }


        #region Checking if user already had a generatorfolder.txt (I mean, Generator Folder configurated)
        public void CheckingGeneratorFolderFile()
        {
            try
            {
                StreamReader sr = new StreamReader("generatorfolder.txt");
                generatorfolder_textbox.Text = sr.ReadToEnd();
                sr.Close();
                AddingBodysFoldersToCombobox();
                whatisit_combobox.Enabled = true;
                StartCopy_Btn.Enabled = true;
                CleanAll_Btn.Enabled = true;

                CheckingNewIconsAlreadyUsed();
            }
            catch (IOException)
            {
                MessageBox.Show("Please, press the button Search and localize your Generator Folder.");
            }

        }
        #endregion

        #region Adding folders found in generator folder in Body combobox (bodysfolders_combobox)
        public void AddingBodysFoldersToCombobox()
        {
            string path = Path.Combine(generatorfolder_textbox.Text, "Variation");

            bodysfolders_combobox.Items.Clear();
            foreach(var d in System.IO.Directory.GetDirectories(path))
            {
                var dir = new DirectoryInfo(d);
                bodysfolders_combobox.Items.Add(dir.Name);
            }
            bodysfolders_combobox.Enabled = true;  
        }
        #endregion

        #region When Body Combobox changes (bodysfolders_combobox)
        private void resourcefolders_combobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string gender = bodysfolders_combobox.Text;
            checkingHowManyFilesIHave(gender, "AccA", accaN);
            checkingHowManyFilesIHave(gender, "AccB", accbN);
            checkingHowManyFilesIHave(gender, "Beard", beardN);
            checkingHowManyFilesIHave(gender, "BeastEars", beastN);
            checkingHowManyFilesIHave(gender, "Cloak", cloakN);
            checkingHowManyFilesIHave(gender, "Clothing", clothN);
            checkingHowManyFilesIHave(gender, "Ears", earsN);
            checkingHowManyFilesIHave(gender, "EyeBrows", eyebrowN);
            checkingHowManyFilesIHave(gender, "Eyes", eyesN);
            checkingHowManyFilesIHave(gender, "Face", faceN);
            checkingHowManyFilesIHave(gender, "FacialMark", facialN);
            checkingHowManyFilesIHave(gender, "FrondHair", fronthairN);
            checkingHowManyFilesIHave(gender, "Glasses", glassesN);
            checkingHowManyFilesIHave(gender, "Mouth", mouthN);
            checkingHowManyFilesIHave(gender, "Nose", noseN);
            checkingHowManyFilesIHave(gender, "RearHair", rearhairN);
            checkingHowManyFilesIHave(gender, "Tail", tailN);
            checkingHowManyFilesIHave(gender, "Wing", wingN);
        }
        #endregion

        #region Method to add how many files I HAVE of every type. This is for when body combobox changes
        public void checkingHowManyFilesIHave(string gender, string item, TextBox whattextbox)
        {
            string path = Path.Combine(generatorfolder_textbox.Text, "Variation");
            path = Path.Combine(path, gender);
            DirectoryInfo dir = new DirectoryInfo(path);
            FileInfo[] files = dir.GetFiles("*" + item + "*.*");

            int numberitems = 0;
            foreach (FileInfo Filesfound in files)
            {
                numberitems = numberitems + 1;
            }

            whattextbox.Text = numberitems.ToString();
        }
        #endregion




        // NEW ICONS METHOD!!!
        #region Button to add new icon to Generator folder
        private void AddNewIconBTN_Click(object sender, EventArgs e)
        {
            // Paso 1 - Carpetas
            // Paso 2 - Cuánto hay de cada
            // Paso 3 - Copiar y renombrar archivo

            //Where are the folders? here!
            string originalPath = Path.Combine(generatorfolder_textbox.Text, "Variation");
            //Now checking every subfolder!

            //This for is for gender: Male, Female and Kid. Items extracted from combobox for Body types
            //At First it where added from folders. They must match.
            for (int genderItem=0; genderItem < bodysfolders_combobox.Items.Count; genderItem++)
            {
                
                // Subpath is the original path XXXX\Variation + Gender, filled in the for just ahead.
                string subPath = Path.Combine(originalPath, bodysfolders_combobox.Items[genderItem].ToString());


                //This next for is for subelements. What I mean?
                //Every part in the Generator have a name. They were already named in whatit combobox.
                for (int elementPart =0; elementPart < whatisit_combobox.Items.Count; elementPart++)
                {
                    //Cambiar luego Borrar por icon_
                    string howMany = HowManyIhaveOfThose(whatisit_combobox.Items[elementPart].ToString(), subPath);                    
                    File.Copy("ICON_NEW.png", Path.Combine(subPath, 
                        "icon_" 
                        + whatisit_combobox.Items[elementPart]) 
                        + "_p" 
                        + howMany 
                        + ".png");

                }

                whatisit_combobox.SelectedIndex = -1;
                bodysfolders_combobox.SelectedIndex = -1;
                ResetAll(WhatIHaveAlready_groupbox);
                ResetAll(fg_groupbox);
                ResetAll(sv_groupbox);
                ResetAll(tv_groupbox);
                ResetAll(icon_groupbox);

                StreamWriter sw = new StreamWriter("newiconsadded.txt", false);
                sw.Close();

                CheckingNewIconsAlreadyUsed();
            }



        }
        #endregion

        #region checking if user already used new icons for his Generator
        public void CheckingNewIconsAlreadyUsed()
        {
            try
            {
                StreamReader sr = new StreamReader("newiconsadded.txt");
                sr.Close();
                AddNewIconBTN.Enabled = false;
            }
            catch (IOException)
            {
                AddNewIconBTN.Enabled = true;
            }

        }
        #endregion

        #region Method to return How many files I have of an item
        public string HowManyIhaveOfThose(string item, string path)
        {
            //MessageBox.Show(path);
            string newPath = Path.Combine(path);
            DirectoryInfo dir = new DirectoryInfo(newPath);
            FileInfo[] files = dir.GetFiles("*" + item + "*.*");

            int numberitems = 0;
            foreach (FileInfo Filesfound in files)
            {
                numberitems = numberitems + 1;
            }

            numberitems = numberitems + 1;

            string n = "";
            if(numberitems < 10)
                n = "0" + numberitems.ToString();
            else
                n = numberitems.ToString();

            return n;
        }
        #endregion
        //END NEW ICONS METHOD!!!





        #region Elements enable or disable in FACE after main combobox changes
        public void fg_enabling(string item)
        {
            ResetAll(fg_groupbox);
            
            switch (item)
            {
                case "AccA":
                    fg_layer1_mask1_textbox.Enabled = true;
                    fg_layer1_mask1_combobox.Enabled = true;
                    fg_layer1_mask1_combobox.Items.AddRange(new String[] { "013", "014", "015" });
                    fg_layer1_mask1_combobox.SelectedIndex = 0;

                    fg_layer1_mask2_textbox.Enabled = true;
                    fg_layer1_mask2_combobox.Enabled = true;
                    fg_layer1_mask2_combobox.Items.AddRange(new String[] { "013", "014", "015" });
                    fg_layer1_mask2_combobox.SelectedIndex = 0;

                    fg_layer1_mask3_textbox.Enabled = true;
                    fg_layer1_mask3_combobox.Enabled = true;
                    fg_layer1_mask3_combobox.Items.AddRange(new String[] { "013", "014", "015" });
                    fg_layer1_mask3_combobox.SelectedIndex = 0;
                    break;

                case "AccB":
                    fg_layer1_mask1_textbox.Enabled = true;
                    fg_layer1_mask1_combobox.Enabled = true;
                    fg_layer1_mask1_combobox.Items.AddRange(new String[] { "016", "017", "018", "019" });
                    fg_layer1_mask1_combobox.SelectedIndex = 0;

                    fg_layer1_mask2_textbox.Enabled = true;
                    fg_layer1_mask2_combobox.Enabled = true;
                    fg_layer1_mask2_combobox.Items.AddRange(new String[] { "016", "017", "018", "019" });
                    fg_layer1_mask2_combobox.SelectedIndex = 0;

                    fg_layer1_mask3_textbox.Enabled = true;
                    fg_layer1_mask3_combobox.Enabled = true;
                    fg_layer1_mask3_combobox.Items.AddRange(new String[] { "016", "017", "018", "019" });
                    fg_layer1_mask3_combobox.SelectedIndex = 0;

                    fg_layer1_mask4_textbox.Enabled = true;
                    fg_layer1_mask4_combobox.Enabled = true;
                    fg_layer1_mask4_combobox.Items.AddRange(new String[] { "016", "017", "018", "019" });
                    fg_layer1_mask4_combobox.SelectedIndex = 0;
                    break;

                case "Beard":
                    fg_layer1_mask1_textbox.Enabled = true;
                    fg_layer1_mask1_combobox.Enabled = true;
                    fg_layer1_mask1_combobox.Items.AddRange(new String[] {"003" });
                    fg_layer1_mask1_combobox.SelectedIndex = 0;
                    break;

                case "BeastEars":
                    fg_layer1_mask1_textbox.Enabled = true;
                    fg_layer1_mask1_combobox.Enabled = true;
                    fg_layer1_mask1_combobox.Items.AddRange(new String[] {"006" });
                    fg_layer1_mask1_combobox.SelectedIndex = 0;
                    break;

                case "Cloak":
                    fg_layer1_mask1_textbox.Enabled = true;
                    fg_layer1_mask1_combobox.Enabled = true;
                    fg_layer1_mask1_combobox.Items.AddRange(new String[] { "011", "012" });
                    fg_layer1_mask1_combobox.SelectedIndex = 0;

                    fg_layer2_mask1_textbox.Enabled = true;
                    fg_layer2_mask1_combobox.Enabled = true;
                    fg_layer2_mask1_combobox.Items.AddRange(new String[] { "011", "012" });
                    fg_layer2_mask1_combobox.SelectedIndex = 0;
                    break;

                case "Clothing":
                    fg_layer1_mask1_textbox.Enabled = true;
                    fg_layer1_mask1_combobox.Enabled = true;
                    fg_layer1_mask1_combobox.Items.AddRange(new String[] { "007", "008", "009", "010" });
                    fg_layer1_mask1_combobox.SelectedIndex = 0;

                    fg_layer1_mask2_textbox.Enabled = true;
                    fg_layer1_mask2_combobox.Enabled = true;
                    fg_layer1_mask2_combobox.Items.AddRange(new String[] { "007", "008", "009", "010" });
                    fg_layer1_mask2_combobox.SelectedIndex = 0;

                    fg_layer2_mask1_textbox.Enabled = true;
                    fg_layer2_mask1_combobox.Enabled = true;
                    fg_layer2_mask1_combobox.Items.AddRange(new String[] { "007", "008", "009", "010" });
                    fg_layer2_mask1_combobox.SelectedIndex = 0;

                    fg_layer2_mask2_textbox.Enabled = true;
                    fg_layer2_mask2_combobox.Enabled = true;
                    fg_layer2_mask2_combobox.Items.AddRange(new String[] { "007", "008", "009", "010" });
                    fg_layer2_mask2_combobox.SelectedIndex = 0;

                    fg_layer2_mask3_textbox.Enabled = true;
                    fg_layer2_mask3_combobox.Enabled = true;
                    fg_layer2_mask3_combobox.Items.AddRange(new String[] { "007", "008", "009", "010" });
                    fg_layer2_mask3_combobox.SelectedIndex = 0;

                    fg_layer2_mask4_textbox.Enabled = true;
                    fg_layer2_mask4_combobox.Enabled = true;
                    fg_layer2_mask4_combobox.Items.AddRange(new String[] { "007", "008", "009", "010" });
                    fg_layer2_mask4_combobox.SelectedIndex = 0;
                    break;

                case "Ears":
                    fg_layer1_mask1_textbox.Enabled = true;
                    fg_layer1_mask1_combobox.Enabled = true;
                    fg_layer1_mask1_combobox.Items.AddRange(new String[] { "001" });
                    fg_layer1_mask1_combobox.SelectedIndex = 0;
                    break;

                case "Eyebrows":
                    fg_layer1_mask1_textbox.Enabled = true;
                    fg_layer1_mask1_combobox.Enabled = true;
                    fg_layer1_mask1_combobox.Items.AddRange(new String[] { "003" });
                    fg_layer1_mask1_combobox.SelectedIndex = 0;

                    fg_layer1_mask2_textbox.Enabled = true;
                    fg_layer1_mask2_combobox.Enabled = true;
                    fg_layer1_mask2_combobox.Items.AddRange(new String[] { "001" });
                    fg_layer1_mask2_combobox.SelectedIndex = 0;
                    break;

                case "Eyes":
                    fg_layer1_mask1_textbox.Enabled = true;
                    fg_layer1_mask1_combobox.Enabled = true;
                    fg_layer1_mask1_combobox.Items.AddRange(new String[] { "002" });
                    fg_layer1_mask1_combobox.SelectedIndex = 0;

                    fg_layer1_mask2_textbox.Enabled = true;
                    break;

                case "Face":
                    fg_layer1_mask1_textbox.Enabled = true;
                    fg_layer1_mask1_combobox.Enabled = true;
                    fg_layer1_mask1_combobox.Items.AddRange(new String[] { "001" });
                    fg_layer1_mask1_combobox.SelectedIndex = 0;
                    break;

                case "FacialMark":
                    fg_layer1_mask1_textbox.Enabled = true;
                    fg_layer1_mask1_combobox.Enabled = true;
                    fg_layer1_mask1_combobox.Items.AddRange(new String[] { "005" });
                    fg_layer1_mask1_combobox.SelectedIndex = 0;
                    break;

                case "FrontHair":
                    fg_layer1_mask1_textbox.Enabled = true;
                    fg_layer1_mask1_combobox.Enabled = true;
                    fg_layer1_mask1_combobox.Items.AddRange(new String[] { "003" });
                    fg_layer1_mask1_combobox.SelectedIndex = 0;
                    break;

                case "Glasses":
                    fg_layer1_mask1_textbox.Enabled = true;
                    fg_layer1_mask1_combobox.Enabled = true;
                    fg_layer1_mask1_combobox.Items.AddRange(new String[] { "020", "021", "022" });
                    fg_layer1_mask1_combobox.SelectedIndex = 0;

                    fg_layer1_mask2_textbox.Enabled = true;
                    fg_layer1_mask2_combobox.Enabled = true;
                    fg_layer1_mask2_combobox.Items.AddRange(new String[] { "020", "021", "022" });
                    fg_layer1_mask2_combobox.SelectedIndex = 0;

                    fg_layer1_mask3_textbox.Enabled = true;
                    fg_layer1_mask3_combobox.Enabled = true;
                    fg_layer1_mask3_combobox.Items.AddRange(new String[] { "020", "021", "022" });
                    fg_layer1_mask3_combobox.SelectedIndex = 0;
                    break;

                case "Mouth":
                    fg_layer1_mask1_textbox.Enabled = true;
                    fg_layer1_mask1_combobox.Enabled = true;
                    fg_layer1_mask1_combobox.Items.AddRange(new String[] { "001" });
                    fg_layer1_mask1_combobox.SelectedIndex = 0;

                    fg_layer1_mask2_textbox.Enabled = true;
                    fg_layer1_mask2_combobox.Enabled = true;
                    fg_layer1_mask2_combobox.Items.AddRange(new String[] { "001" });
                    fg_layer1_mask2_combobox.SelectedIndex = 0;

                    fg_layer1_mask3_textbox.Enabled = true;
                    fg_layer1_mask3_combobox.Enabled = true;
                    fg_layer1_mask3_combobox.Items.AddRange(new String[] { "001" });
                    fg_layer1_mask3_combobox.SelectedIndex = 0;
                    break;

                case "Nose":
                    fg_layer1_mask1_textbox.Enabled = true;
                    fg_layer1_mask1_combobox.Enabled = true;
                    fg_layer1_mask1_combobox.Items.AddRange(new String[] { "001" });
                    fg_layer1_mask1_combobox.SelectedIndex = 0;
                    break;

                case "RearHair":
                    fg_layer1_mask1_textbox.Enabled = true;
                    fg_layer1_mask1_combobox.Enabled = true;
                    fg_layer1_mask1_combobox.Items.AddRange(new String[] { "003", "004" });
                    fg_layer1_mask1_combobox.SelectedIndex = 0;

                    fg_layer2_mask1_textbox.Enabled = true;
                    fg_layer2_mask1_combobox.Enabled = true;
                    fg_layer2_mask1_combobox.Items.AddRange(new String[] { "003" });
                    fg_layer2_mask1_combobox.SelectedIndex = 0;

                    fg_layer2_mask2_textbox.Enabled = true;
                    fg_layer2_mask2_combobox.Enabled = true;
                    fg_layer2_mask2_combobox.Items.AddRange(new String[] { "003" });
                    fg_layer2_mask2_combobox.SelectedIndex = 0;

                    break;

                case "Tail":
                    
                    break;

                case "Wing":
                    
                    break;

            }
        }
        #endregion

        #region Elements enable or disable in BATTLECHARAS after main combobox changes
        public void sv_enabling(string item)
        {
            ResetAll(sv_groupbox);

            if (item != "Eyebrows" &&
                item != "Eyes" &&
                item != "Face" &&
                item != "Mouth" &&
                item != "Nose")
            {
                sv_layer1_sprite_textbox.Enabled = true;
                sv_layer1_mask_textbox.Enabled = true;
                
            }

            if(item == "Cloak" ||
               item =="Clothing")
            {
                sv_layer2_sprite_textbox.Enabled = true;
                sv_layer2_mask_textbox.Enabled = true;
            }
                      
        }
        #endregion

        #region Elements enable or disable in CHARACTERS and DAMAGED after main combobox changes
        public void tv_enabling(string item)
        {
            ResetAll(tv_groupbox);



            if (item != "Eyebrows" &&
                item != "Eyes" &&
                item != "Face" &&
                item != "Mouth" &&
                item != "Nose")
            {
                tv_layer1_sprite_textbox.Enabled = true;
                tv_layer1_mask_textbox.Enabled = true;

                tvd_damagedlayer_sprite_textbox.Enabled = true;
                tvd_damagedlayer_mask_textbox.Enabled = true;                

            }

            if (item == "Beard" ||
                item == "Cloak" ||
                item == "Clothing" ||
                item == "FrontHair" ||
                item == "RearHair" ||
                item == "Tail" ||
                item == "Wing")
            {
                tv_layer2_sprite_textbox.Enabled = true;
                tv_layer2_mask_textbox.Enabled = true;
            }

        }
        #endregion

        #region Elements enable or disable in ICONS after main combobox changes
        public void icon_enabling(string item)
        {
            ResetAll(icon_groupbox);
            icon_sprite_textbox.Enabled = true;
        }
        #endregion

        #region Method to RESET ALL textboxes and comboboxes in groupboxes
        public void ResetAll(Control groupbox)
        {
            foreach (Control childControl in groupbox.Controls)
            {
                if (childControl is TextBox)
                {
                    childControl.ResetText();
                    childControl.Enabled = false;
                }
                else if (childControl is ComboBox)
                {
                    (childControl as ComboBox).Items.Clear();
                    (childControl as ComboBox).Enabled = false;
                    (childControl as ComboBox).SelectedIndex = -1;
                    (childControl as ComboBox).Text = "";
                }
            }
        }
        #endregion





        #region What happens when you drag and drop a file in the textboxes
        private void DragEnterImage(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.All;
            else
                e.Effect = DragDropEffects.None;
        }


        private void DragDropImage(object sender, DragEventArgs e)
        {
            string[] s = (string[])e.Data.GetData(DataFormats.FileDrop, false);
            (sender as TextBox).Text = Path.GetFileName(s[0]);
            CalculatePathFolder(s[0]);
        }
        #endregion

        #region After drag the first file, the main folder of your resources will be visible.
        public void CalculatePathFolder(string fullPathString)
        {
            if (fullPathTextBox.Text == "")
            {
                fullPathTextBox.Text = Path.GetDirectoryName(fullPathString);
            }
        }
        #endregion

        #region Button to search Generator Folder
        private void search_btn_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog generatorfolder = new FolderBrowserDialog();
            generatorfolder.ShowDialog();
            generatorfolder_textbox.Text = generatorfolder.SelectedPath;

            StreamWriter sw = new StreamWriter("generatorfolder.txt", false);
            sw.Write(generatorfolder_textbox.Text);
            sw.Close();

            CheckingGeneratorFolderFile();
            CheckingNewIconsAlreadyUsed();
            AddingBodysFoldersToCombobox();
        }

        #endregion





        #region Button to start adding files
        private void StartCopy_Btn_Click(object sender, EventArgs e)
        {

            if (icon_sprite_textbox.Text == "")
            {
                MessageBox.Show("Every resource added must have an icon");
            }
            else
            {
                if (whatisit_combobox.Text != "" && bodysfolders_combobox.Text != "")
                {
                    string elementNumber = CheckNumberForElementBeforeRename();

                    // Naming Faces
                    RenameAFile(fg_layer1_mask1_textbox.Text, FindFaceName(elementNumber, "1", fg_layer1_mask1_combobox.Text, false), "Face");
                    RenameAFile(fg_layer1_mask2_textbox.Text, FindFaceName(elementNumber, "2", fg_layer1_mask2_combobox.Text, false), "Face");
                    RenameAFile(fg_layer1_mask3_textbox.Text, FindFaceName(elementNumber, "3", fg_layer1_mask3_combobox.Text, false), "Face");
                    RenameAFile(fg_layer1_mask4_textbox.Text, FindFaceName(elementNumber, "4", fg_layer1_mask4_combobox.Text, false), "Face");

                    RenameAFile(fg_layer2_mask1_textbox.Text, FindFaceName(elementNumber, "1", fg_layer2_mask1_combobox.Text, false), "Face");
                    RenameAFile(fg_layer2_mask2_textbox.Text, FindFaceName(elementNumber, "2", fg_layer2_mask2_combobox.Text, false), "Face");
                    RenameAFile(fg_layer2_mask3_textbox.Text, FindFaceName(elementNumber, "3", fg_layer2_mask3_combobox.Text, false), "Face");
                    RenameAFile(fg_layer2_mask4_textbox.Text, FindFaceName(elementNumber, "4", fg_layer2_mask4_combobox.Text, false), "Face");



                    // Naming BattleCharas
                    RenameAFile(sv_layer1_sprite_textbox.Text, FindBattleCharaName(elementNumber, false, false), "SV");
                    RenameAFile(sv_layer1_mask_textbox.Text, FindBattleCharaName(elementNumber, true, false), "SV");
                    RenameAFile(sv_layer2_sprite_textbox.Text, FindBattleCharaName(elementNumber, false, true), "SV");
                    RenameAFile(sv_layer2_mask_textbox.Text, FindBattleCharaName(elementNumber, true, true), "SV");

                    // Naming Char Set
                    RenameAFile(tv_layer1_sprite_textbox.Text, FindCharacterName(elementNumber, false, false), "TV");
                    RenameAFile(tv_layer1_mask_textbox.Text, FindCharacterName(elementNumber, true, false), "TV");
                    RenameAFile(tv_layer2_sprite_textbox.Text, FindCharacterName(elementNumber, false, true), "TV");
                    RenameAFile(tv_layer2_mask_textbox.Text, FindCharacterName(elementNumber, true, true), "TV");

                    // Naming Character Damaged
                    RenameAFile(tvd_damagedlayer_sprite_textbox.Text, FindCharacterDamagedName(elementNumber, false), "TVD");
                    RenameAFile(tvd_damagedlayer_mask_textbox.Text, FindCharacterDamagedName(elementNumber, true), "TVD");
                    
                    // Naming Icons
                    RenameAFile(icon_sprite_textbox.Text, FindIconName(elementNumber), "Variation");
                }
                
               /* ResetAll(fg_groupbox);
                ResetAll(sv_groupbox);
                ResetAll(tv_groupbox);
                ResetAll(icon_groupbox);*/
                ResetAll(WhatIHaveAlready_groupbox);
                //fullPathTextBox.Text = "";
               // whatisit_combobox.SelectedIndex = -1;
                bodysfolders_combobox.SelectedIndex = -1;
            }
        }
        #endregion
        
        #region To check number needed to add new resource
        public string CheckNumberForElementBeforeRename()
        {
            string theNumberNeeded = ""; 
            switch (whatisit_combobox.Text)
            {
                case "AccA":
                    theNumberNeeded = accaN.Text;
                    break;

                case "AccB":
                    theNumberNeeded = accbN.Text;
                    break;

                case "Beard":
                    theNumberNeeded = beardN.Text;
                    break;

                case "BeastEars":
                    theNumberNeeded = beastN.Text;
                    break;

                case "Cloak":
                    theNumberNeeded = cloakN.Text;
                    break;

                case "Clothing":
                    theNumberNeeded = clothN.Text;
                    break;

                case "Ears":
                    theNumberNeeded = earsN.Text;
                    break;

                case "Eyebrows":
                    theNumberNeeded = eyebrowN.Text;
                    break;

                case "Eyes":
                    theNumberNeeded = eyesN.Text;
                    break;

                case "Face":
                    theNumberNeeded = faceN.Text;
                    break;

                case "FacialMark":
                    theNumberNeeded = facialN.Text;
                    break;

                case "FrontHair":
                    theNumberNeeded = fronthairN.Text;
                    break;

                case "Glasses":
                    theNumberNeeded = glassesN.Text;
                    break;

                case "Mouth":
                    theNumberNeeded = mouthN.Text;
                    break;

                case "Nose":
                    theNumberNeeded = noseN.Text;
                    break;

                case "RearHair":
                    theNumberNeeded = rearhairN.Text;
                    break;

                case "Tail":
                    theNumberNeeded = tailN.Text;
                    break;

                case "Wing":
                    theNumberNeeded = wingN.Text;
                    break;

            }

            int x = Int32.Parse(theNumberNeeded) + 1;
            theNumberNeeded = x.ToString();
            if (x < 10)
                return "0" + theNumberNeeded;
            else
                return theNumberNeeded;
        }

        #endregion

        #region to find FACE names
        public string FindFaceName(string number, string maskNumber, string colour, bool extraLayer)
        {
            string element = whatisit_combobox.Text;

            //Checking the exceptions about naming.
            if (whatisit_combobox.Text == "Cloak" ||
                whatisit_combobox.Text == "Clothing" ||
                whatisit_combobox.Text == "RearHair")
            {
                if (extraLayer == false)
                {
                    element = whatisit_combobox.Text + "1";
                }
                else
                {
                    element = whatisit_combobox.Text + "2";
                }
            }

            //Exception for Clothing, because the mask numbers start in 3 until 6 if they are extralayer
            if (whatisit_combobox.Text == "Clothing" && extraLayer == true)
            {
                int mm = Int32.Parse(maskNumber) + 2;
                maskNumber = mm.ToString();
            }

            
            string part1 = "FG_"; // How it start
            string part2 = element + "_"; // What isit
            string part3 = "p" + number + "_"; // The name itself,
            string part4 = "c" + maskNumber +  "_"; // the Mask number
            string part5 = "m" + colour; // The colour choosen
            string part6 = ".png"; //The extension

            return part1 + part2 + part3 + part4 + part5 + part6;
        }
        #endregion

        #region Method to find ICONS names
        public string FindIconName(string number)
        {
            string part1 = "icon_"; // How it start
            string part2 = whatisit_combobox.Text + "_"; // What isit
            string part3 = "p"+ number; // The name itself,
            string part4 = ".png"; //The extension

            return part1 + part2 + part3 + part4;
        }
        #endregion

        #region Method to find BATTLECHARA names
        public string FindBattleCharaName(string number, bool itIsAMask, bool extraLayer)
        {
            string element = whatisit_combobox.Text;
            string mask = "";

            //Checking the exceptions about naming.
            if(whatisit_combobox.Text == "Cloak" ||
                whatisit_combobox.Text == "Clothing")
            {
                if (extraLayer == false)
                {
                    element = whatisit_combobox.Text + "1";
                }
                else
                {
                    element = whatisit_combobox.Text + "2";
                }
            }

            //Checking another exception. RearHair must finish in 1
            if (whatisit_combobox.Text == "RearHair")
            {
                element = whatisit_combobox.Text + "1";
            }

            //Checking if the file is a mask. In that case we must add _c
            if (itIsAMask == true)
            {
                mask = "_c";
            }


            string part1 = "SV_"; // How it start
            string part2 = element + "_"; // What isit
            string part3 = "p" + number; // The name itself,
            string part4 = mask;           
            string part5 = ".png"; //The extension

            return part1 + part2 + part3 + part4 + part5;

        }
        #endregion

        #region Method to find CHARACTER names
        public string FindCharacterName(string number, bool itIsAMask, bool extraLayer)
        {
            string element = whatisit_combobox.Text;
            string mask = "";


            //Checking the exceptions about naming.
            if (whatisit_combobox.Text == "Beard" ||
                whatisit_combobox.Text == "Cloak" ||
                whatisit_combobox.Text == "Clothing" ||
                whatisit_combobox.Text == "FrontHair" ||
                whatisit_combobox.Text == "RearHair" ||
                whatisit_combobox.Text == "Tail" ||
                whatisit_combobox.Text == "Wing")
            {
                if (extraLayer == false)
                {
                    element = whatisit_combobox.Text + "1";
                }
                else
                {
                    element = whatisit_combobox.Text + "2";
                }
            }
            
            //Checking if the file is a mask. In that case we must add _c
            if (itIsAMask == true)
            {
                mask = "_c";
            }

            
            string part1 = "TV_"; // How it start
            string part2 = element + "_"; // What isit
            string part3 = "p" + number; // The name itself,
            string part4 = mask;
            string part5 = ".png"; //The extension

            return part1 +part2 + part3 + part4 + part5;
        }
        #endregion

        #region Method to find CHARACTER DAMAGED names
        public string FindCharacterDamagedName(string number, bool itIsAMask)
        {
            string mask = "";

            //Checking if the file is a mask. In that case we must add _c
            if (itIsAMask == true)
            {
                mask = "_c";
            }
            
            string part1 = "TVD_"; // How it start
            string part2 = whatisit_combobox.Text + "_"; // What isit
            string part3 = "p" + number; // The name itself,
            string part4 = mask;
            string part5 = ".png"; //The extension

            return part1 + part2 + part3 + part4 + part5;
        }

        #endregion

        #region Method to rename a file
        public void RenameAFile(string originalFile, string newFileName, string subFolder)
        {

            if (debugMode == true)
            {
                newFileName = "NewFileToTry   " + newFileName;
            }


            // Looking folder of original files
            string originalPathInTextBox = fullPathTextBox.Text;
            string originalPath = Path.Combine(originalPathInTextBox, originalFile);

            // Looking for Destination Folder
            // generatorfolder_textbox + subFolder + BodyFolder + newFileName
            string resourceFolder = Path.Combine(generatorfolder_textbox.Text, subFolder);
            string bodyFolder = Path.Combine(resourceFolder, bodysfolders_combobox.Text);
            string finalPath = Path.Combine(bodyFolder, newFileName);





           // MessageBox.Show(originalPath);
           // MessageBox.Show(finalPath);
            try
            {
                File.Copy(originalPath, finalPath);
            }
            catch (Exception ex)
            { }

            
        }
        #endregion


        #region Method to clean all fields
        private void CleanAll_Btn_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want clean all fields?", "Warning!", MessageBoxButtons.YesNo);

            if (dr == DialogResult.Yes)
            { 
              fullPathTextBox.Text = "";
              whatisit_combobox.SelectedIndex = -1;
              bodysfolders_combobox.SelectedIndex = -1;
              ResetAll(fg_groupbox);
              ResetAll(sv_groupbox);
              ResetAll(tv_groupbox);
              ResetAll(icon_groupbox);
              ResetAll(WhatIHaveAlready_groupbox);
            }
        }
        #endregion
    }
}





#region Helps for open files. Just my personal backup.

// Open dialog
/*
OpenFileDialog myfile = new OpenFileDialog();
myfile.ShowDialog();
textBox1.Text =  myifle.FileName;
*/

// read File
/*
StreamReader sr = new StreamReader(textBox1.Text); // Será la ruta?
textBox2.Text = sr.ReadToEnd();
sr.Close();
*/

// Write File
/*
StreamWriter sw = new StreamWriter(textBox2.Text, false);
sw.WriteLine(textbox2.Text);
sw.Close();
*/

#endregion
#region todos los casos del switch
/*
    switch (item)
            {
                case "AccA":
                    sv_sprite1_textbox.Enabled = true;
                    sv_mask1_textbox.Enabled = true;
                    break;

                case "AccB":

                    break;

                case "Beard":

                    break;

                case "BeastEars":

                    break;

                case "Cloak":

                    break;

                case "Clothing":

                    break;

                case "Ears":

                    break;

                case "Eyebrows":

                    break;

                case "Eyes":

                    break;

                case "Face":

                    break;

                case "FacialMark":

                    break;

                case "FrontHair":

                    break;

                case "Glasses":

                    break;

                case "Mouth":

                    break;

                case "Nose":

                    break;

                case "RearHair": 

                    break;

                case "Tail":

                    break;

                case "Wing":

                    break;

            }*/
#endregion
#region what number I need for naming files? OBSOLETE
/*
public string LookingForNumberNeeded(string item)
{
    string number = "";
    switch (item)
    {
        case "AccA":
            number = accaN.Text;
            break;

        case "AccB":
            number = accbN.Text;
            break;

        case "Beard":
            number = beardN.Text;
            break;

        case "BeastEars":
            number = beastN.Text;
            break;

        case "Cloak":
            number = cloakN.Text;
            break;

        case "Clothing":
            number = clothN.Text;
            break;

        case "Ears":
            number = earsN.Text;
            break;

        case "Eyebrows":
            number = eyebrowN.Text;
            break;

        case "Eyes":
            number = eyesN.Text;
            break;

        case "Face":
            number = faceN.Text;
            break;

        case "FacialMark":
            number = facialN.Text;
            break;

        case "FrontHair":
            number = fronthairN.Text;
            break;

        case "Glasses":
            number = glassesN.Text;
            break;

        case "Mouth":
            number = mouthN.Text;
            break;

        case "Nose":
            number = noseN.Text;
            break;

        case "RearHair":
            number = rearhairN.Text;
            break;

        case "Tail":
            number = tailN.Text;
            break;

        case "Wing":
            number = wingN.Text;
            break;

    }

    return number;
}
*/
#endregion
#region Adding numbers from file.txt to textboxs OBSOLETE
/*
public void AddDatesToTextBoxs(int i, string date)
{
    i = i + 1;
    switch (i)
    {
        case 1:
            accaN.Text = date;
            break;

        case 2:
            accbN.Text = date;
            break;

        case 3:
            beardN.Text = date;
            break;

        case 4:
            beastN.Text = date;
            break;

        case 5:
            cloakN.Text = date;
            break;

        case 6:
            clothN.Text = date;
            break;

        case 7:
            earsN.Text = date;
            break;

        case 8:
            eyebrowN.Text = date;
            break;

        case 9:
            eyesN.Text = date;
            break;

        case 10:
            faceN.Text = date;
            break;

        case 11:
            facialN.Text = date;
            break;

        case 12:
            fronthairN.Text = date;
            break;

        case 13:
            glassesN.Text = date;
            break;

        case 14:
            mouthN.Text = date;
            break;

        case 15:
            noseN.Text = date;
            break;

        case 16:
            rearhairN.Text = date;
            break;

        case 17:
            tailN.Text = date;
            break;

        case 18:
            wingN.Text = date;
            break;

    }
}
*/
#endregion
#region Reading items.txt OBSOLETE
/*  public void ReadFile()
  {
      var lines = File.ReadAllLines("items.txt");
      for (var i = 0; i < lines.Length; i += 1)
      {
          AddDatesToTextBoxs(i, lines[i]);
      }
  }*/
#endregion
#region All text boxes only allow for NUMBERS! (Already obsolete, because textbox are read from file).
/*
private void whatname_KeyPress(object sender, KeyPressEventArgs e)
{ CharAllowed(sender, e); }

private void whatpart_KeyPress(object sender, KeyPressEventArgs e)
{ CharAllowed(sender, e); }

private void accaN_KeyPress(object sender, KeyPressEventArgs e)
{ CharAllowed(sender, e); }

private void accbN_KeyPress(object sender, KeyPressEventArgs e)
{ CharAllowed(sender, e); }

private void clothN_KeyPress(object sender, KeyPressEventArgs e)
{ CharAllowed(sender, e); }

private void earsN_KeyPress(object sender, KeyPressEventArgs e)
{ CharAllowed(sender, e); }

private void eyebrowsN_KeyPress(object sender, KeyPressEventArgs e)
{ CharAllowed(sender, e); }

private void eyesN_KeyPress(object sender, KeyPressEventArgs e)
{ CharAllowed(sender, e); }

private void faceN_KeyPress(object sender, KeyPressEventArgs e)
{ CharAllowed(sender, e); }

private void facialN_KeyPress(object sender, KeyPressEventArgs e)
{ CharAllowed(sender, e); }

private void fronthairN_KeyPress(object sender, KeyPressEventArgs e)
{ CharAllowed(sender, e); }

private void glassesN_KeyPress(object sender, KeyPressEventArgs e)
{ CharAllowed(sender, e); }

private void mouthN_KeyPress(object sender, KeyPressEventArgs e)
{ CharAllowed(sender, e); }

private void noseN_KeyPress(object sender, KeyPressEventArgs e)
{ CharAllowed(sender, e); }

private void rearhairN_KeyPress(object sender, KeyPressEventArgs e)
{ CharAllowed(sender, e); }

private void tailN_KeyPress(object sender, KeyPressEventArgs e)
{ CharAllowed(sender, e); }

private void wingN_KeyPress(object sender, KeyPressEventArgs e)
{ CharAllowed(sender, e); }

private void beardN_KeyPress(object sender, KeyPressEventArgs e)
{ CharAllowed(sender, e); }
*/
#endregion
#region Method for check if key pressed is a number or not. (Already obsolete, because textbox are read from file).
/*
public bool CharAllowed(object sender, KeyPressEventArgs e)
{
e.Handled = false;
//Check if it is number or not
if (!char.IsControl(e.KeyChar) &&
     !char.IsDigit(e.KeyChar))
{
    e.Handled = true;
}

return e.Handled;
}
*/
#endregion





